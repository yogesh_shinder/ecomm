﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eComm.Web.Startup))]
namespace eComm.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
